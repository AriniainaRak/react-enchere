// import './Login.css';
import { useState } from "react";

function Login() {

    function verifier() {
        const email = document.getElementById("mail").value;
        const pwd = document.getElementById("pwd").value;
        window.location.href = "/Verifier/" + email + "/" + pwd;
    }


    function accueil(event) {
        window.location.href = "/";
    }
    return (
        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100 p-t-30 p-b-50">
                    <span class="login100-form-title p-b-41">
                        Account Login
                    </span>
                    <div class="login100-form validate-form p-b-33 p-t-5">

                        <div class="wrap-input100 validate-input" data-validate="Enter username">
                            <input class="input100" type="text" id="mail" name="username" placeholder="User name" value="Rakoto@gmail.com" />
                            <span class="focus-input100" data-placeholder="&#xe82a;"></span>
                        </div>

                        <div class="wrap-input100 validate-input" data-validate="Enter password">
                            <input class="input100" type="password" id="pwd" name="pass" placeholder="Password" value="rakoto123" />
                            <span class="focus-input100" data-placeholder="&#xe80f;"></span>
                        </div>

                        <div class="container-login100-form-btn m-t-32">
                            <button onClick={verifier} class="login100-form-btn">
                                Login
                            </button>
                            <button onClick={accueil} class="login100-form-btn">
                                Accueil
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login;