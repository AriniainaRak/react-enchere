import { useParams } from "react-router-dom";

function Verifier() {
  const { email } = useParams();
  const { password } = useParams();

  var xhr = new XMLHttpRequest();
  var url = "http://enchere-ws-production.up.railway.app/login?email=" + email + "&mdp=" + password;

  xhr.open("GET", url, true);
  xhr.send();

  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      var result = JSON.parse(xhr.responseText);
      console.log(result);
      if (result["message"] === "Correct") {
        sessionStorage.setItem("token", result["token"]);
        sessionStorage.setItem("idClient", result["id"]);
        window.location.href = "/";
      } else {
        console.log(result["message"]);
        window.location.href = "/Login";
      }
    }
  }
}
export default Verifier;