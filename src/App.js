import './App.css';
import Login from './Login';
import Accueil from './Accueil'
import Recherche from './Recherche'
import Liste from './Liste'
import Fiche from './Fiche'
import FichePerso from './FichePerso'
import Verifier from './Verifier'
import { Route, Routes } from 'react-router-dom';
import Historique from './Historique';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Accueil />}></Route>
        <Route path="/Login" element={<Login />}></Route>
        <Route path="/Recherche" element={<Recherche />}></Route>
        <Route path="/Liste" element={<Liste />}></Route>
        <Route path="/Fiche/:idEnchere" element={<Fiche />}></Route>
        <Route path="/Historique" element={<Historique />}></Route>
        <Route path="/FichePerso/:idEnchere" element={<FichePerso />}></Route>
        <Route path="/Verifier/:email/:password" element={<Verifier />}></Route>
      </Routes>
    </div>
  );
}

export default App;
