import axios from "axios";
import React, { useEffect, useState } from "react";

function Historique(){

    function accueil() {
        window.location.href = "/";
    }

    function fiche(id) {
        console.log("enchere clicker : " + id);
        window.location.href = "/Fiche/" + id;
    }
    const idclient = sessionStorage.getItem("idClient");
    console.log(idclient);
    const [data, setData] = useState([]);
    useEffect(() => {
        axios.get('http://enchere-ws-production.up.railway.app/enchere/historique?idClient='+idclient)
            .then(res => {
                setData(res.data);
            })
            .catch(error => {
                console.error(error);
            });
    }, [])
    console.log(data);
    return (


        <div className="liste">
            <h1>Historique des encheres</h1>
            <table class="table">
                <thead>
                    <tr>
                        <th>description</th>
                        <th>date Enchere</th>
                        <th>Mise</th>
                        <th>statue</th>
                    </tr>
                </thead>
                <tbody>
                    {data.map(enchere => (
                        <tr>
                            <>

                                <td key={enchere.id}>{enchere.description}</td>
                                <td key={enchere.id}>{enchere.date}</td>
                                <td key={enchere.id}>{enchere.mise}</td>
                                <td key={enchere.id}>{enchere.statue}</td>
                            </>
                        </tr>
                    ))}
                </tbody>
            </table>
                        <button type="button" class="btn btn-light" onClick={accueil}>Accueil</button>
        </div>
    )
}
export default Historique; 