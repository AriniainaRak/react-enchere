// import './style.css';

function Accueil() {
    function signin() {
        window.location.href = "/Login";
    }
    function search() {
        window.location.href = "/Recherche";
    }
    function liste() {
        window.location.href = "/Liste";
    }
    function historique() {
        window.location.href = "/Historique";
    }
    return (
        <div class="box">
            <nav class="nav nav-pills nav-fill">
                <a class="nav-item nav-link" href="/Login">Login</a>
                <a class="nav-item nav-link" href="/Recherche">Recherche</a>
                <a class="nav-item nav-link" href="/Liste">Liste</a>
                <a class="nav-item nav-link" href="/Historique">Historique</a>
            </nav>
        </div>
    )
}

export default Accueil;
