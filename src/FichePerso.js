// import './fiche.css';
import React, { useState, useEffect } from "react";
import axios from 'axios';
import { useParams } from "react-router-dom";

function FichePerso() {

    function retour() {
        window.location.href = "/Historique";
    }

    const { idEnchere } = useParams();
    console.log(idEnchere);
    const [data, setData] = useState([]);
    useEffect(() => {
        axios.get('http://enchere-ws-production.up.railway.app/enchere/detailenchere?idEnchere=' + idEnchere)
            .then(res => {
                setData(res.data);
            })
            .catch(error => {
                console.error(error);
            });
    }, [])
    console.log(data);

    return (
        <div class="container">
            {data.map(enchere => (
                <>
                    <h2>Enchere {idEnchere}</h2>
                    <p key={enchere.id}>Durree : {enchere.duree} min</p>
                    <p key={enchere.id}>produit : {enchere.produit}</p>
                    <p key={enchere.id}>Prix Minimum : {enchere.prixmin} Ariary</p>
                    <p key={enchere.id}>Date debut enchere : {enchere.datedebut}</p>
                    <button type="button" class="btn btn-light" onClick={retour}>Retour</button>
                </>
            ))}
        </div>
    )
}

export default FichePerso;