// import './fiche.css';
import React, { useState, useEffect } from "react";
import axios from 'axios';
import { useParams } from "react-router-dom";

function Fiche() {
    const [result, setResult] = useState([]);

    function getXhr() {
        return new XMLHttpRequest();
    }
    function encherir() {
            const montant = document.getElementById("montant").value;
            console.log("montant ecrit : " + montant + " Ariary");
            const idclient = sessionStorage.getItem("idClient");
            var xhr = getXhr();
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    var response = JSON.parse(xhr.responseText);
                    console.log(response);
                    window.location.href = "/Liste";
                }
            }

            xhr.open("GET", "http://enchere-ws-production.up.railway.app/enchere/encherir?idEnchere=" + idEnchere + "&montant=" + montant + "&idClient=" + idclient, true);
            xhr.send();
            window.location.href = "/Liste";
        }


    function retour() {
        window.location.href = "/Liste";
    }

    const { idEnchere } = useParams();
    const [data, setData] = useState([]);
    useEffect(() => {
        axios.get('http://localhost:8080/enchere/detailenchere?idEnchere=' + idEnchere)
            .then(res => {
                setData(res.data);
            })
            .catch(error => {
                console.error(error);
            });
    }, [])
    console.log(data);

    return (
        <div className="fiche">
            {data.map(enchere => (
                <>
                    <h2>Enchere {idEnchere}</h2>
                    <p key={enchere.id}>Durree : {enchere.duree}</p>
                    <p key={enchere.id}>produit : {enchere.produit}</p>
                    <p key={enchere.id}>Prix Minimum : {enchere.prixmin} Ariary</p>
                    <p key={enchere.id}>Date debut enchere : {enchere.datedebut}</p>
                    <input type="text" id="montant" />
                    <button type="button" class="btn btn-light" onClick={encherir}>Encherir</button>
                    <button type="button" class="btn btn-light" onClick={retour}>Retour</button>
                </>
            ))}
        </div>

    )
}

export default Fiche;