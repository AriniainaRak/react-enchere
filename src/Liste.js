import React, { useState, useEffect } from "react";
import axios from 'axios';


function Liste() {
    function accueil() {
        window.location.href = "/";
    }

    function fiche(id) {
        console.log("enchere clicker : " + id);
        const idCLient = sessionStorage.getItem("idClient");
        if(idCLient === null){
            window.location.href = "/Login";
        }else{
            window.location.href = "/Fiche/" + id;
        }
    }

    const [data, setData] = useState([]);
    useEffect(() => {
        axios.get('http://enchere-ws-production.up.railway.app/enchere/liste')
            .then(res => {
                setData(res.data);
            })
            .catch(error => {
                console.error(error);
            });
    }, [])
    console.log(data);
    return (


        <div className="liste">
            <h1>Liste des encheres</h1>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">produit</th>
                        <th scope="col">duree</th>
                        <th scope="col">date debut</th>
                        <th scope="col">prix minimum</th>
                    </tr>
                </thead>
                <tbody>
                    {data.map(enchere => (
                        <tr>
                            <>

                                <td key={enchere.id}>{enchere.produit}</td>
                                <td key={enchere.id}>{enchere.duree}</td>
                                <td key={enchere.id}>{enchere.datedebut}</td>
                                <td key={enchere.id}>{enchere.prixmin}</td>
                                <td key={enchere.id}><button type="button" class="btn btn-light" onClick={() => fiche(enchere.idEnchere)}>Detail</button></td>
                            </>
                        </tr>
                    ))}
                </tbody>
            </table>
                        <button type="button" class="btn btn-light" onClick={accueil}>Accueil</button>
        </div>
    )

}

export default Liste;