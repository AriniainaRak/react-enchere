// import './style.css';
import { useState } from "react";


function Recherche() {
    const [result, setResult] = useState([]);
    const idClient = sessionStorage.getItem("idClient")
    const encherir = (id) => {

        if(idClient === null || idClient === 0){
            window.location.href = "/Fiche/"+id;
        }else{
            console.log("idenchere cliquer et envoyer :" + id);
            window.location.href = "/Fiche/"+id;
        }
    }

    function accueil() {
        window.location.href = "/";
    }

    function getXhr() {
        return new XMLHttpRequest();
    }

    function recherche(Event) {
        const critere = document.getElementById("critere").value;
        const categ = document.getElementById("categ").value;
        console.log(critere);
        var xhr = getXhr();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                var response = JSON.parse(xhr.responseText);
                console.log(response);
                setResult(response);
            }
        }

        xhr.open("GET", "http://enchere-ws-production.up.railway.app/enchere/search?critere=" + critere + "&categ=" + categ, true);
        xhr.send();
    }

    return (
        <div className="container">
            <h2>Recherche</h2>
            <p>Critere : <input id="critere" type="text" name="critere" /></p>
            <p>Categorie : <input id="categ" type="text" name="critere" /></p>
            <button class="btn btn-light" onClick={recherche}>Search</button>
            <button class="btn btn-light" onClick={accueil}>Accueil</button>
            <table class="table">
                <thead>
                    <tr>
                        <th>Produit</th>
                        <th>Duree</th>
                        <th>Date debut</th>
                        <th>Prix minimum</th>
                    </tr>
                </thead>
                <tbody id="result">
                    {
                        result.map(res => (
                            < >
                                <tr key={res.idEnchere}>
                                    <td key={res.idEnchere}> {res.produit} </td>
                                    <td key={res.idEnchere}>{res.duree}  </td>
                                    <td key={res.idEnchere}> {res.datedebut} </td>
                                    <td key={res.idEnchere}> {res.prixmin}  </td>
                                    <td key={res.idEnchere}>
                                        <button type="button" class="btn btn-light" key={res.idEnchere} onClick={() => {
                                            encherir(res.idEnchere)
                                        }}>Encherir</button>
                                    </td>
                                </tr>
                            </>
                        ))
                    }

                </tbody>
            </table>
        </div>
    )
}

export default Recherche;